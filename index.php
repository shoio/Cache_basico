<?php 
class Cache{
	private $cache;

	public function setVar($nome, $valor){
		$this->readCache();
		$this->cache->$nome = $valor;
		$this->saveCache();
	}

	public function getVar($nome){
		$this->readCache();
		return $this->cache->$nome;
	}

	private function readCache(){
		$this->cache = new stdClass(); //cria um obj vazio
		if (file_exists('cache.cache')) {
			$this->cache = jason_decode(file_get_contents('cache.cache'));
		}
	}

	private function saveCache(){
		file_put_contents("cache.cache", jason_encode($this->cache));
	}

}

$cache = new Cache();
$cache->setVar("idade", 90);

echo "Minha idade: ".$cache->getVar("idade");
